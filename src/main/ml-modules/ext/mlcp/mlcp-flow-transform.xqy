xquery version "1.0-ml";

module namespace mlcpFlow = "http://abnamro.nl/data-hub/mlcp-flow-transform";

import module namespace flow = "http://marklogic.com/data-hub/flow-lib"
at "/data-hub/4/impl/flow-lib.xqy";

declare namespace hub = "http://marklogic.com/data-hub";
declare namespace es = 'http://marklogic.com/entity-services';
declare variable $NAMESPACE := 'http://nl.abnamro.com/cre';
declare option xdmp:mapping "false";

declare function mlcpFlow:transform(
  $content as map:map,
  $context as map:map
) as map:map*
{
  let $uri := map:get($content, "uri")
  let $root := fn:substring-before(fn:upper-case(fn:tokenize($uri, "/")[fn:last()]),'-')
  let $checksum := fn:substring-before(fn:tokenize($uri, "-")[fn:last()],'.xml')

  let $transform-string := map:get($context, 'transform_param')
  let $options-string := replace($transform-string, '^.*(options=\{.*\}).*$', '$1')
  let $parsed-transform-string :=
    if ($transform-string = $options-string) then
      $transform-string
    else
      fn:replace($transform-string, 'options=\{.*\}', '')
  let $params := map:new(
    for $pair in $parsed-transform-string ! fn:tokenize(., ",")
    let $parts := fn:tokenize($pair, "=")
    return
      if(fn:not(fn:empty($parts[1]))) then
        map:entry($parts[1], $parts[2])
      else
        ()
  )

  let $job-id := (map:get($params, "job-id"),map:get($params, "jobid"), map:get($params, "jobId"),sem:uuid-string())[1]
  let $entity-name := map:get($params, 'entity-name') ! xdmp:url-decode(.)
  let $flow-name := map:get($params, 'flow-name') ! xdmp:url-decode(.)
  let $flow := flow:get-flow(
    $entity-name,
    $flow-name,
    "input"
  )

  let $_ :=
    if ($flow) then ()
    else
      fn:error((), "RESTAPI-SRVEXERR", "The specified flow " || map:get($params, "flow") || " is missing.")

  (: configure the options :)
  let $opts := map:new(
    let $opt-parts := fn:tokenize($options-string, "=")

    return
      map:entry($opt-parts[1], $opt-parts[2])
  )
  let $options as map:map := (
    map:get($opts, "options") ! xdmp:unquote(.)/object-node(),
    map:map()
  )[1]

  let $_ :=
    $options
    => map:with('root', $root)
    => map:with('checksum', $checksum)
  let $_ := flow:set-default-options($options, $flow)

  let $mainFunc := flow:get-main($flow/hub:main)
  (: this can throw, but we want MLCP to know about problems, so let it :)
  let $envelope := flow:run-flow($job-id, $flow, $uri, map:get($content, "value"), $options, $mainFunc)

  (: write the trace for the current identifier :)
  (:
    let $item-context := map:get($flow:context-queue, $uri)
    let $_ := trace:write-trace($item-context)
  :)

  let $hid := xdmp:with-namespaces(("ns", $NAMESPACE), $envelope/es:envelope/es:headers/ns:hid/xs:string(.))
  let $sid := xdmp:with-namespaces(("ns", $NAMESPACE), $envelope/es:envelope/es:headers/ns:source/xs:string(.))

  let $_ := (
    map:put($content, "value", $envelope),
    map:put($context, "collections", (map:get($context, "collections"), fn:concat($NAMESPACE, "/", fn:lower-case($sid), "/", $hid)))
  )
  return
    $content
};
